<?php
session_start();
include 'connection.php'; 
include 'process/helpers.php';
$time = $_SERVER['REQUEST_TIME'];

/**
* for a two hour timeout, specified in seconds
*/
$timeout_duration = 7200;

/**
* Here we look for the user's LAST_ACTIVITY timestamp. If
* it's set and indicates our $timeout_duration has passed,
* blow away any previous $_SESSION data and start a new one.
*/
if (isset($_SESSION['LAST_ACTIVITY']) && 
   ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
    session_unset();
    session_destroy();
    session_start();
}

/**
* Finally, update LAST_ACTIVITY so that our timeout
* is based on it and not the user's login time.
*/
$_SESSION['LAST_ACTIVITY'] = $time;
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Kementerian Agama BIMAS Buddha RI Kota Batam</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="public/images/icons/logo-014.png" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/fonts/fontawesome-5.0.8/css/fontawesome-all.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/fonts/iconic/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/css/util.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/css/main.css">
	<!--===============================================================================================-->
	<!--===============================================================================================-->
	<script src="public/vendor/jquery/jquery-3.2.1.min.js"></script>
</head>
<style>
	.logo img {
		width: 40% !important;
		height: auto !important;
	}
	.modal-backdrop {
		opacity: 0.5 !important;
		background-color: black;
	}
</style>

<body class="animsition">
<?php 
if(isset($_SESSION['id_profil'])): 
	$id_profile = $_SESSION['id_profil'];
    $result_profile = mysqli_query($conn, "SELECT * FROM profil_login where id_profil =$id_profile");
    while($userlogin_profil = mysqli_fetch_array($result_profile))
    {
        $nama_lengkap = $userlogin_profil['nama_lengkap'];
        $alamat       = $userlogin_profil["alamat"];
        $ttl          = $userlogin_profil["ttl"];
        $telp         = $userlogin_profil["telp"];
        
	}
else: 
		$nama_lengkap = NULL;
        $alamat       = NULL;
        $ttl          = NULL;
        $telp         = NULL;
endif;
if($nama_lengkap != NULL || $alamat != NULL || $ttl != NULL || $telp != NULL): 
	$urllinkmenu = TRUE;
else:
	$urllinkmenu = FALSE;
endif;
?>

	<!-- Header -->
	<header>
		<!-- Header desktop -->
		<div class="container-menu-desktop">


			<!-- Header Mobile -->
			<div class="wrap-header-mobile">
				<!-- Logo moblie -->
				<div class="logo-mobile">
					<a href="index.html"><img src="public/images/icons/logo-01.jpg" alt="IMG-LOGO"></a>
				</div>

				<!-- Button show menu -->
				<div class="btn-show-menu-mobile hamburger hamburger--squeeze m-r--8">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>

			<!-- Menu Mobile -->
			<div class="menu-mobile">
				<ul class="topbar-mobile">


					<li class="left-topbar">
					<?php if(isset($_SESSION["login"])): ?>
						<a href="process/logout.php" class="left-topbar-item">
							Log Out
						</a>
					<?php else: ?>
						<a href="login.php" class="left-topbar-item">
							Log in
						</a>
					<?php endif; ?>

					</li>
				</ul>

				<ul class="main-menu-m">
					<li>
						<a href="index.php?page=home">Beranda</a>
					</li>

					<li>
						<a href="index.php?page=news">Berita</a>
					</li>
					<?php 
					if(isset($_SESSION['role_id'])):
						if($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 2):
					?>

					<li>
						<a href="index.php?page=profil">Profil</a>
					</li>
					<?php 
						endif;
					endif;
					if(!isset($_SESSION["login"])): 
                    ?>
					<li>
						<a href="index.php?page=daftarpandita">Daftar Pandita</a>
                    </li>
                    <?php 
					endif;							
					if(isset($_SESSION['role_id'])):
						if($_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 1):
					?>

					<li>
						<a href="<?php if($urllinkmenu == TRUE):?>index.php?page=buat_berita<?php  else: ?>#<?php endif; ?>">Buat Berita</a>
					</li>
					<?php
						endif;
					endif;
                        if(isset($_SESSION['role_id'])):
                            if($_SESSION['role_id'] == 1):
                    ?>
					<li>
						<a href="#">Laporan</a>
						<ul class="sub-menu-m">
							<li><a href="<?php if($urllinkmenu == TRUE):?>index.php?page=admin<?php  else: ?>#<?php endif; ?>">Admin</a></li>
							<li><a href="<?php if($urllinkmenu == TRUE):?>index.php?page=acc_pandita<?php  else: ?>#<?php endif; ?>">Laporan Acc Pandita</a></li>
							<li><a href="<?php if($urllinkmenu == TRUE):?>index.php?page=berita_acc<?php  else: ?>#<?php endif; ?>">Laporan Acc Berita Pandita</a></li>
							<li><a href="<?php if($urllinkmenu == TRUE):?>index.php?page=pandita<?php  else: ?>#<?php endif; ?>">Laporan List Pandita</a></li>
							<li><a href="<?php if($urllinkmenu == TRUE):?>index.php?page=list_berita<?php  else: ?>#<?php endif; ?>">Laporan List Berita</a></li>
						</ul>

						<span class="arrow-main-menu-m">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</span>
                    </li>
					<?php 
							elseif($_SESSION['role_id'] == 2): 
					?>
					<li><a href="<?php if($urllinkmenu == TRUE):?>index.php?page=list_berita<?php  else: ?>#<?php endif; ?>">List Berita</a></li>
					<?php
                            endif;
                        endif;
                    ?>

				</ul>
			</div>

			<!--  -->
			<div class="wrap-logo container">
				<!-- Logo desktop -->

				<div class="banner-header" style="width:100%">
					<a href="#"><img src="public/images/icons/logo-013.png" alt="IMG"></a>
				</div>
			</div>

			<!--  -->
			<div class="wrap-main-nav">
				<div class="main-nav">
					<!-- Menu desktop -->
					<nav class="menu-desktop">
						<a class="logo-stick" href="index.html">
							<img src="public/images/icons/logo-01.jpg" alt="LOGO">
						</a>

						<ul class="main-menu">
							<li class="<?php if(isset($_GET['page'])): if($_GET['page'] === 'home'): ?>main-menu-active<?php endif; endif;?>">
								<a href="index.php?page=home">Beranda</a>
							</li>

							<li class="mega-menu-item <?php if(isset($_GET['page'])): if($_GET['page'] === 'news'): ?>main-menu-active<?php endif; endif;?>">
								<a href="index.php?page=news">Berita</a>
                            </li>
                            

                            <?php 
                                if(isset($_SESSION['role_id'])):
                                    if($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 2):
                            ?>
							<li class="mega-menu-item <?php if(isset($_GET['page'])): if($_GET['page'] === 'profil'): ?>main-menu-active<?php endif; endif;?>">
								<a href="index.php?page=profil">Profile</a>
                            </li>
                            <?php
                                    endif;
                                endif; 

                                if(!isset($_SESSION["login"])): 
                            ?>
							<li class="mega-menu-item <?php if(isset($_GET['page'])): if($_GET['page'] === 'daftarpandita'): ?>main-menu-active<?php endif; endif;?>">
								<a href="index.php?page=daftarpandita">Daftar Pandita</a>
                            </li>
                            <?php 
								endif;
							if(isset($_SESSION['role_id'])):
								if($_SESSION['role_id'] == 2 || $_SESSION['role_id'] == 1):
							?>

							<li class="mega-menu-item <?php if(isset($_GET['page'])): if($_GET['page'] === 'buat_berita'): ?>main-menu-active<?php endif; endif;?>">
							<a href="<?php if($urllinkmenu == TRUE):?>index.php?page=buat_berita<?php else: ?>#<?php endif; ?>">Buat Berita</a>
                            </li>
							<?php
								endif;
							endif;
							if(isset($_SESSION['role_id'])):
								if($_SESSION['role_id'] == 1):
                            ?>

							<li>
								<a href="#">Laporan</a>
								<ul class="sub-menu main-menu-active">
									<li><a href="<?php if($urllinkmenu == TRUE):?>index.php?page=admin<?php else: ?>#<?php endif; ?>">Admin</a></li>
									<li class="main-menu-active"><a href="<?php if($urllinkmenu == TRUE):?>index.php?page=acc_pandita<?php else: ?>#<?php endif; ?>">Laporan Acc Pandita</a></li>
									<li><a href="<?php if($urllinkmenu == TRUE):?>index.php?page=berita_acc<?php else: ?>#<?php endif; ?>">Laporan Acc Berita Pandita</a></li>
									<li><a href="<?php if($urllinkmenu == TRUE):?>index.php?page=pandita<?php else: ?>#<?php endif; ?>">Laporan List Pandita</a></li>							 
									<li><a href="<?php if($urllinkmenu == TRUE):?>index.php?page=list_berita<?php else: ?>#<?php endif; ?>">Laporan List Berita</a></li>
								</ul>
                            </li>
                            <?php
								elseif($_SESSION['role_id'] == 2): 
							?> 
							<li class="mega-menu-item <?php if(isset($_GET['page'])): if($_GET['page'] === 'list_berita'): ?>main-menu-active<?php endif; endif;?>">
							<a href="<?php if($urllinkmenu == TRUE):?>index.php?page=list_berita<?php else: ?>#<?php endif; ?>">List Berita</a>
                            </li>
							<?php
								endif;
							endif; 
							
                                if(isset($_SESSION["login"])):
                            ?>
                            <li>
								<a href="process/logout.php">Logout</a>
                            </li>
                            <?php 
                                else:
                            ?>
							<li>
								<a href="login.php">Login</a>
                            </li>
                            <?php 
                                endif;
                            ?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>
	<?php 
		if(isset($_GET['page'])):
			$page = $_GET['page'];
			if($page === 'home'): 
				include 'pages/home.php';
			elseif($page === 'daftarpandita'): 
				include 'pages/daftarpandita.php';
			elseif($page === 'news'):
				include 'pages/news.php';
			elseif($page === 'profil'):
				if(isset($_SESSION['role_id'])):
					if($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 2): 
						include 'pages/admin/profile.php';
					else: 
						echo 'Andat tidak bisa akses ke halaman ini';
					endif;
				else: 
					echo 'Andat tidak bisa akses ke halaman ini';
				endif;
			elseif($page === 'buat_berita'):
				if(isset($_SESSION['role_id'])):
					if($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 2): 
						if($urllinkmenu == TRUE):
							include 'pages/admin/buatberita.php';
						else: 
							echo 'anda harus ke profile dan isi alamt telp, tempat tgl lahir dan nama lengkap';
						endif;
					else: 
						echo 'Andat tidak bisa akses ke halaman ini';
					endif;
				else: 
					echo 'Andat tidak bisa akses ke halaman ini';
				endif;
			elseif($page === 'acc_pandita'):
				if(isset($_SESSION['role_id'])):
					if($_SESSION['role_id'] == 1): 
						if($urllinkmenu == TRUE):
							include 'pages/admin/laporan_acc_pandita.php';
						else: 
							echo 'anda harus ke profile dan isi alamt telp, tempat tgl lahir dan nama lengkap';
						endif;
					else: 
						echo 'Andat tidak bisa akses ke halaman ini';
					endif;
				else: 
					echo 'Andat tidak bisa akses ke halaman ini';
				endif;
			elseif($page === 'berita_acc'):
				if(isset($_SESSION['role_id'])):
					if($_SESSION['role_id'] == 1): 
						if($urllinkmenu == TRUE):
							include 'pages/admin/laporan_berita_acc.php';
						else: 
							echo 'anda harus ke profile dan isi alamt telp, tempat tgl lahir dan nama lengkap';
						endif;
					else: 
						echo 'Andat tidak bisa akses ke halaman ini';
					endif;
				else: 
					echo 'Andat tidak bisa akses ke halaman ini';
				endif;
			elseif($page === 'pandita'): 
				if(isset($_SESSION['role_id'])):
					if($_SESSION['role_id'] == 1): 
						if($urllinkmenu == TRUE):
							include 'pages/admin/list_pandita.php';
						else: 
							echo 'anda harus ke profile dan isi alamt telp, tempat tgl lahir dan nama lengkap';
						endif;
					else: 
						echo 'Andat tidak bisa akses ke halaman ini';
					endif;
				else: 
					echo 'Andat tidak bisa akses ke halaman ini';
				endif;
			elseif($page === 'detail_berita'):
				include 'pages/detail_berita.php';
			elseif($page === 'list_berita'): 
				if(isset($_SESSION['role_id'])):
					if($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 2): 
						if($urllinkmenu == TRUE):
							include 'pages/admin/list_berita.php';
						else: 
							echo 'anda harus ke profile dan isi alamt telp, tempat tgl lahir dan nama lengkap';
						endif;
					else: 
						echo 'Andat tidak bisa akses ke halaman ini';
					endif;
				else: 
					echo 'Andat tidak bisa akses ke halaman ini';
				endif;
			elseif($page === 'admin'):
				if(isset($_SESSION['role_id'])):
					if($_SESSION['role_id'] == 1): 
						if($urllinkmenu == TRUE):
							include 'pages/admin/admin.php';
						else: 
							echo 'anda harus ke profile dan isi alamt telp, tempat tgl lahir dan nama lengkap';
						endif;
					else: 
						echo 'Andat tidak bisa akses ke halaman ini';
					endif;
				else: 
					echo 'Andat tidak bisa akses ke halaman ini';
				endif;
			endif;
		else:
			include 'pages/home.php';
		endif;

	?>


	<!-- Feature post -->


	<!-- Post -->


	<!-- Banner -->
	<div class="container">
		<div class="flex-c-c">
            <!-- footer -->
		</div>
	</div>

	<!-- Footer -->
	<footer>

		<div class="bg11">
			<div class="container size-h-4 flex-c-c p-tb-15">
				<span class="f1-s-1 cl0 txt-center">
					Copyright © 

					<a href="#" class="f1-s-1 cl10 hov-link1">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;<script>
							document.write(new Date().getFullYear());
						</script> All rights reserved</a>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</span>
			</div>
		</div>
	</footer>

	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<span class="fas fa-angle-up"></span>
		</span>
	</div>

	<!-- Modal Video 01-->
	<div class="modal fade" id="modal-video-01" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document" data-dismiss="modal">
			<div class="close-mo-video-01 trans-0-4" data-dismiss="modal" aria-label="Close">&times;</div>

			<div class="wrap-video-mo-01">
				<div class="video-mo-01">
					<iframe src="https://www.youtube.com/embed/wJnBTPUQS5A?rel=0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>


	<!--===============================================================================================-->
	<script src="public/vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="public/vendor/bootstrap/js/popper.js"></script>
	<script src="public/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="public/js/main.js"></script>

</body>

</html>