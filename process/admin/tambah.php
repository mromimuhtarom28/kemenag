<?php 
session_start();
require '../../connection.php';

if(isset($_POST['tambah_admin'])):
    $username       = $_POST['username'];
    $pwdthisaccount = $_POST['passwordthisaccount'];
    $pwd            = $_POST['password'];
    $usernameacoount = $_SESSION['username'];
    
    if(is_null($username)): 
        $_SESSION["alert"] = 'Mohon maaf username kosong wajib diisi';
        header("Location:../../index.php?page=admin");
    elseif(is_null($pwdthisaccount)):
        $_SESSION["alert"] = 'Mohon maaf password yang sedang login kosong wajib diisi';
        header("Location:../../index.php?page=admin");
    elseif(is_null($pwd)):
        $_SESSION["alert"] = 'Mohon maaf password untuk akun ini kosong wajib diisi';
        header("Location:../../index.php?page=admin");
    else:
        
        $acoountlogin_query = "Select * from login where username='$usernameacoount'";
        $hasil_acoountlogin = $conn->query($acoountlogin_query);
        while ($data_account = $hasil_acoountlogin->fetch_assoc()):
            $pwddatabase          = $data_account['password'];
        endwhile;


        $usernameexist_query = "Select * from login where username='$username'";
        $hasil_username = $conn->query($usernameexist_query);
        $username_tersedia = array();
        while ($data_username = $hasil_username->fetch_assoc()):
            // disertakan pula link untuk menghapus data berdasarkan ID nya
            $username_tersedia[] .= $data_username['username'];
        endwhile;
        if(in_array($username, $username_tersedia)):
            $_SESSION["alert"] = 'username sudah digunakan silahkan pakai username lain';
            header("Location:../../index.php?page=admin");
        else: 
            if(!password_verify($pwdthisaccount, $pwddatabase)): 
                $_SESSION["alert"] = 'Mohon maaf password yang sedang login salah, silahkan coba lagi';
                header("Location:../../index.php?page=admin");
            else:
                $queryselect = "SELECT * FROM profil_login order by id_profil desc limit 1";

                $hasil = $conn->query($queryselect);
                while ($data = $hasil->fetch_assoc())
                {
                    // disertakan pula link untuk menghapus data berdasarkan ID nya
                    $id = $data['id_profil'];
                }
                if(isset($id)):
                    $idlogin = $id + 1;
                else:
                    $idlogin = 1;
                endif;
                $password = password_hash($pwd, PASSWORD_DEFAULT);
                $insertlogin = "INSERT INTO login (username, password, id_role, id_profil) values ('$username', '$password', 1, $idlogin)";
                $insertprofillogin = "INSERT INTO profil_login (id_profil) values ('$idlogin')";
                $execlogin       = $conn->query($insertlogin);
                $execprofillogin = $conn->query($insertprofillogin);
                if($execlogin === TRUE):
                    $_SESSION["success"] = 'Tambah data admin sukses';
                    header("Location:../../index.php?page=admin");
                else:
                    $_SESSION["alert"] = $conn->error;
                    header("Location:../../index.php?page=admin");
                endif;

                if($execprofillogin === TRUE):
                    $_SESSION["success"] = 'Tambah data admin sukses';
                    header("Location:../../index.php?page=admin");
                else:
                    $_SESSION["alert"] = $conn->error;
                    header("Location:../../index.php?page=admin");
                endif;
            endif;
        endif;
    endif;

else: 
    $_SESSION["alert"] = 'Mohon maaf terjadi kesalahan';
    header("Location:../../index.php?page=admin");
endif;
?>