<?php 
session_start();
require '../../connection.php';

if(isset($_POST['delete_admin'])):
    $pk = $_POST['pk'];
    
    if(is_null($pk)): 
        $_SESSION["alert"] = 'Mohon maaf pk kosong wajib diisi';
        header("Location:../../index.php?page=admin");
    else:
        $deletelogin       = "DELETE FROM login WHERE id_profil=$pk";
        $deleteprofillogin = "DELETE FROM profil_login WHERE id_profil=$pk";
        $execlogin       = $conn->query($deletelogin);
        $execprofillogin = $conn->query($deleteprofillogin);
        if($execlogin === TRUE):
            $_SESSION["success"] = 'Hapus data admin sukses';
            header("Location:../../index.php?page=admin");
        else:
            $_SESSION["alert"] = $conn->error;
            header("Location:../../index.php?page=admin");
        endif;

        if($execprofillogin === TRUE):
            $_SESSION["success"] = 'Hapus data admin sukses';
            header("Location:../../index.php?page=admin");
        else:
            $_SESSION["alert"] = $conn->error;
            header("Location:../../index.php?page=admin");
        endif;
    endif;

else: 
    $_SESSION["alert"] = 'Mohon maaf terjadi kesalahan';
    header("Location:../../index.php?page=admin");
endif;
?>