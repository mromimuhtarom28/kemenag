<?php 
session_start();
require '../../connection.php';

if(isset($_POST['tambah_admin'])):
    $pk              = $_POST['pk'];
    $pwdthisaccount  = $_POST['passwordthisaccount'];
    $pwd             = $_POST['password'];
    $usernameacoount = $_SESSION['username'];
    
    if(is_null($pk)): 
        $_SESSION["alert"] = 'Mohon maaf pk kosong wajib diisi';
        header("Location:../../index.php?page=admin");
    elseif(is_null($pwdthisaccount)):
        $_SESSION["alert"] = 'Mohon maaf password yang sedang login kosong wajib diisi';
        header("Location:../../index.php?page=admin");
    elseif(is_null($pwd)):
        $_SESSION["alert"] = 'Mohon maaf password untuk akun ini kosong wajib diisi';
        header("Location:../../index.php?page=admin");
    else:
        
        $acoountlogin_query = "Select * from login where username='$usernameacoount'";
        $hasil_acoountlogin = $conn->query($acoountlogin_query);
        while ($data_account = $hasil_acoountlogin->fetch_assoc()):
            $pwddatabase          = $data_account['password'];
        endwhile;

        if(!password_verify($pwdthisaccount, $pwddatabase)): 
            $_SESSION["alert"] = 'Mohon maaf password yang sedang login salah, silahkan coba lagi';
            header("Location:../../index.php?page=admin");
        else:
            $password    = password_hash($pwd, PASSWORD_DEFAULT);
            $updatelogin = "update login set password='$password' where id_profil=$pk";
            $execlogin   = $conn->query($updatelogin);
            if($execlogin === TRUE):
                $_SESSION["success"] = 'Ubah password admin sukses';
                header("Location:../../index.php?page=admin");
            else:
                $_SESSION["alert"] = $conn->error;
                header("Location:../../index.php?page=admin");
            endif;
        endif;
    endif;
else: 
    $_SESSION["alert"] = 'Mohon maaf terjadi kesalahan';
    header("Location:../../index.php?page=admin");
endif;
?>