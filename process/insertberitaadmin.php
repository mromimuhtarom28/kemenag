<?php 
session_start();
require '../connection.php';
if(isset($_POST['submit'])):
    $judul  = $_POST['judul'];
    $konten = $_POST['konten'];
    $idlogin = $_SESSION['userid'];
    $headline = $_POST['headline'];
    $datenow = date('Y-m-d H:i:s');




    $queryselect = "SELECT * FROM berita order by id_berita desc limit 1";

    $hasil = $conn->query($queryselect);
    while ($data = $hasil->fetch_assoc())
    {
        // disertakan pula link untuk menghapus data berdasarkan ID nya
        $id = $data['id_berita'];
    }
    
    if(isset($id)):
        $idberita = $id + 1;
    else:
        $idberita = 1;
    endif;

    if($_FILES['file']['name'] != NULL):
        $nama   = $_FILES['file']['name'];
        $ekstensi_diperbolehkan = array('jpg', 'png', 'jpeg');
        $x                      = explode('.', $nama);
        $ekstensi               = strtolower(end($x));
        $ukuran                 = $_FILES['file']['size'];
        $file_tmp               = $_FILES['file']['tmp_name'];
        $nama_file_unik         = $idberita.'.'.$ekstensi;
        list($width, $height)   = getimagesize($_FILES["file"]["tmp_name"]);
        
        if(in_array($ekstensi, $ekstensi_diperbolehkan) == true)
        {
            if($ukuran < 1044070)
            {           
                // if($width <= 1000 && $height <= 1000):
                    if (move_uploaded_file($file_tmp, '../image/berita_pandita/'.$nama_file_unik))
                    {

                        $insertberita = "INSERT INTO berita (id_berita, judul, headline, isi, id_login, tgl_terbit, status, gambar, tgl_disetujui) values ($idberita, '$judul', '$headline', '$konten', $idlogin, '$datenow', 2, '$nama_file_unik', '$datenow')";

                        if($conn->query($insertberita) === TRUE):
                            $_SESSION["success"] = 'Insert data berita sukses';
                            header("Location:../index.php?page=buat_berita");
                        else:
                            $_SESSION["alert"] = $conn->error;
                            header("Location:../index.php?page=buat_berita");
                        endif;
    
                    }
                    else
                    {
                        $_SESSION['alert'] = "Gagal Upload File";
                        header('location:../index.php?page=buat_berita');
                    }
                // else:
                //     $_SESSION['alert'] = "ukuran gambar tidak boleh melebihi 1000 x 1000";
                //     header('location:../index.php?page=buat_berita');  
                // endif;
            }
            else
            {       
                $_SESSION['alert'] ='Ukuran file terlalu besar';
                header('location:../index.php?page=buat_berita');
            }
        }
        else
        {       
            $_SESSION['alert'] = 'Ekstensi file tidak di perbolehkan';
            header('location:../index.php?page=buat_berita');
        }
    else: 
        $insertberita = "INSERT INTO berita (id_berita, judul, headline, isi, id_login, tgl_terbit, status, gambar) values ($idberita, '$judul', '$headline', '$konten', $idlogin, '$datenow', 1, '')";

        if($conn->query($insertberita) === TRUE):
            $_SESSION["success"] = 'Insert data berita sukses';
            header("Location:../index.php?page=buat_berita");
        else:
            $_SESSION["alert"] = $conn->error;
            header("Location:../index.php?page=buat_berita");
        endif;
    endif;
endif;
?>