<?php 
session_start();
require '../connection.php';
if(isset($_POST['submit'])):
    $fullname = $_POST['fullname'];
    $noktp    = $_POST['noktp'];
    $nohp     = $_POST['nohp'];
    $tgllahir = $_POST['tgllahir'];
    $username = $_POST['username'];
    $pwd      = $_POST['katasandi'];
    $confirmpwd = $_POST['konfirmsandi'];
    $email    = $_POST['email'];
    $alamat   = $_POST['alamat'];

    if($pwd != $confirmpwd):
        $_SESSION["alert"] = 'password dan konfirmasi password tidak sama silahkan coba';
        header("Location:../index.php?page=daftarpandita");
    endif;
    $queryselect = "SELECT * FROM profil_login order by id_profil desc limit 1";

    $hasil = $conn->query($queryselect);
    while ($data = $hasil->fetch_assoc())
    {
        // disertakan pula link untuk menghapus data berdasarkan ID nya
        $id = $data['id_profil'];
    }
    
    if(isset($id)):
        $idlogin = $id + 1;
    else:
        $idlogin = 1;
    endif;

    $usernameexist_query = "Select * from login";
    $hasil_username = $conn->query($usernameexist_query);
    $username_tersedia = array();
    while ($data_username = $hasil_username->fetch_assoc())
    {
        // disertakan pula link untuk menghapus data berdasarkan ID nya
        $username_tersedia[] .= $data_username['username'];
    }
    if(in_array($username, $username_tersedia)):
        $_SESSION["alert"] = 'username sudah digunakan silahkan pakai username lain';
        header("Location:../index.php?page=daftarpandita");
    endif;



        $ekstensi_diperbolehkan = array('png', 'jpg', 'PNG', 'JPG', 'jpeg', 'JPEG');
        $nama                   = $_FILES['file']['name'];
        $x                      = explode('.', $nama);
        $ekstensi               = strtolower(end($x));
        $ukuran                 = $_FILES['file']['size'];
        $file_tmp               = $_FILES['file']['tmp_name'];
        $nama_file_unik         = $idlogin.'.png';
        list($width, $height)   = getimagesize($_FILES["file"]["tmp_name"]);
        
        if(in_array($ekstensi, $ekstensi_diperbolehkan) == true)
        {
            if($ukuran < 1044070)
            {           
                    if (move_uploaded_file($file_tmp, '../image/profil/'.$nama_file_unik))
                    {


                        $password = password_hash($pwd, PASSWORD_DEFAULT);
                        $insertlogin = "INSERT INTO login (username, password, id_role, id_profil) values ('$username', '$password', 2, $idlogin)";
                        $insertprofillogin = "INSERT INTO profil_login (nama_lengkap, alamat, ttl, telp) values ('$fullname', '$alamat', '$tgllahir', '$nohp')";
                        $insertpandita = "INSERT INTO pandita (no_ktp, email, id_profil, status) values ($noktp, '$email', $idlogin, 1)";


                            if($conn->query($insertlogin) === TRUE):
                                if($conn->query($insertprofillogin) === TRUE):
                                    if($conn->query($insertpandita) === TRUE):
                                        $_SESSION["success"] = 'Insert data sukses tunggu email masuk dari admin kementrian agama';
                                        header("Location:../index.php?page=daftarpandita");
                                    else:
                                        $_SESSION["alert"] = $conn->error;
                                        header("Location:../index.php?page=daftarpandita");
                                    endif;
                                else:
                                    $_SESSION["alert"] = 'Insert data profil failed';
                                    header("Location:../index.php?page=daftarpandita");
                                endif;
                            else:
                                $_SESSION["alert"] = 'Insert data login failed';
                                header("Location:../index.php?page=daftarpandita");
                            endif;
                    }
                    else
                    {
                        $_SESSION['alert'] = "Gagal Upload File";
                        header('location:../index.php?page=daftarpandita');
                    }
            }
            else
            {       
                $_SESSION['alert'] ='Ukuran file terlalu besar';
                header('location:../index.php?page=daftarpandita');
            }
        }
        else
        {       
            $_SESSION['alert'] = 'Ekstensi file tidak di perbolehkan';
            header('location:../index.php?page=daftarpandita');
        }
endif;    

    



?>