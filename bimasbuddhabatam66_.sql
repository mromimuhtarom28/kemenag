-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 01 Mar 2021 pada 07.42
-- Versi server: 10.3.25-MariaDB-0ubuntu0.20.04.1
-- Versi PHP: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bimasbuddhabatam66_`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(5) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `headline` text NOT NULL,
  `isi` text NOT NULL,
  `id_login` int(5) NOT NULL,
  `tgl_terbit` datetime NOT NULL,
  `tgl_disetujui` datetime NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1,
  `gambar` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE `login` (
  `id_login` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_role` int(5) NOT NULL,
  `id_profil` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`id_login`, `username`, `password`, `id_role`, `id_profil`) VALUES
(1, 'admin', '$2y$10$SJn.nNwIa3O/KqqVWitF7.xicyW8.q.lkb/3brQaI18muaPzThFtK', 1, 1),
(2, 'admin3', '$2y$10$SJn.nNwIa3O/KqqVWitF7.xicyW8.q.lkb/3brQaI18muaPzThFtK', 1, 2),
(8, 'jaya', '$2y$10$P0P58sqw3zf1rJGROVR3v.HFleO7HFdH2aWAhv7oAtiGOq/FgQmy2', 2, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pandita`
--

CREATE TABLE `pandita` (
  `id_pandita` int(5) NOT NULL,
  `no_ktp` int(16) NOT NULL,
  `email` varchar(255) NOT NULL,
  `id_profil` int(5) NOT NULL,
  `status` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pandita`
--

INSERT INTO `pandita` (`id_pandita`, `no_ktp`, `email`, `id_profil`, `status`) VALUES
(3, 2147483647, 'BakuHantamCrew1@gmail.com', 3, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil_login`
--

CREATE TABLE `profil_login` (
  `id_profil` int(5) NOT NULL,
  `nama_lengkap` text NOT NULL,
  `alamat` text NOT NULL,
  `ttl` text NOT NULL,
  `telp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `profil_login`
--

INSERT INTO `profil_login` (`id_profil`, `nama_lengkap`, `alamat`, `ttl`, `telp`) VALUES
(1, 'Admin', 'admin', '', '082392191962'),
(2, 'Wan Halim', 'tiban', '1998-11-20', '082392191962'),
(7, 'jaya', 'Aceh', '2020-11-28', '083833571194');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE `role` (
  `id_role` int(5) NOT NULL,
  `nama_role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id_role`, `nama_role`) VALUES
(1, 'admin'),
(2, 'pandita');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indeks untuk tabel `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_login`);

--
-- Indeks untuk tabel `pandita`
--
ALTER TABLE `pandita`
  ADD PRIMARY KEY (`id_pandita`);

--
-- Indeks untuk tabel `profil_login`
--
ALTER TABLE `profil_login`
  ADD PRIMARY KEY (`id_profil`);

--
-- Indeks untuk tabel `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `login`
--
ALTER TABLE `login`
  MODIFY `id_login` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `pandita`
--
ALTER TABLE `pandita`
  MODIFY `id_pandita` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `profil_login`
--
ALTER TABLE `profil_login`
  MODIFY `id_profil` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
