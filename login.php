<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="public/images/icons/logo-014.png" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="style/login.css">
    <?php include 'connection.php'; ?>
</head>
<!-- <body>
  <div class="header">

  </div> -->
<style>
@import "bourbon";

body {
	background: #eee !important;	
}

.wrapper {	
	margin-top: 80px;
  margin-bottom: 80px;
}

.form-signin {
  max-width: 380px;
  padding: 15px 35px 45px;
  margin: 0 auto;
  background-color: #fff;
  border: 1px solid rgba(0,0,0,0.1);  

  .form-signin-heading,
	.checkbox {
	  margin-bottom: 30px;
	}

	.checkbox {
	  font-weight: normal;
	}

	.form-control {
	  position: relative;
	  font-size: 16px;
	  height: auto;
	  padding: 10px;
		@include box-sizing(border-box);

		&:focus {
		  z-index: 2;
		}
	}

	input[type="text"] {
	  margin-bottom: -1px;
	  border-bottom-left-radius: 0;
	  border-bottom-right-radius: 0;
	}

	input[type="password"] {
	  margin-bottom: 20px;
	  border-top-left-radius: 0;
	  border-top-right-radius: 0;
	}
}
</style>

  <div class="wrapper">
    <form class="form-signin" action="process/login.php" method="POST">       
      <h2 class="form-signin-heading" align="center">Silahkan Login Terlebih Dahulu</h2>
      <div class="width:80%;height:30%;margin-bottom:3px;" class="border border-dark">
        <img src="public/images/icons/logo-014.png" style="width:100%;height:auto;margin-bottom:15px;" alt="">
      </div>
      <?php
      session_start();
      if(isset($_SESSION['error'])):
      ?>
            <div class="alert alert-danger">
                <?= $_SESSION["error"]; ?>
            </div>
            <?php unset($_SESSION['error']); ?> 
        <?php 
            endif;
        ?>
      <input type="text" class="form-control" style="margin-bottom:10px;" name="username" placeholder="Username" required="" autofocus="" />
      <input type="password" class="form-control" name="password" placeholder="Password" required=""/>      
      <button class="btn btn-lg btn-primary btn-block" name="login" type="submit">Login</button>   
    </form>
  </div>

  <script src="jquery.slim.min.js"></script>
  <script src="bootstrap/dist/js/bootstrap.js"></script>
</body>

</html>