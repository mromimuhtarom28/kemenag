<link rel="stylesheet" href="public/css/button.css">
<section class="bg0">
    <div class="container border" style="background-color:#DCDCDC;margin-bottom:20px;margin-top:20px;padding-top:50px;padding-bottom:50px;">
        <div align="center" style="margin-bottom:20px;">
            <h1><b>Pendaftaran Pandita Kota Batam</b></h1>
        </div>
        <?php 
            if(isset($_SESSION["success"])):
        ?>
            <div class="alert alert-success">
                <?= $_SESSION["success"]; ?>
            </div>
            <?php unset($_SESSION['success']); ?>
        <?php 
            endif;

            if(isset($_SESSION['alert'])):
        ?>
            <div class="alert alert-danger">
                <?= $_SESSION["alert"]; ?>
            </div>
            <?php unset($_SESSION['alert']); ?> 
        <?php 
            endif;

        ?>


        <form action="process/insertpandita.php" method="post" enctype="multipart/form-data">
            <div align="center">
                Foto Profile anda<br>
                <img src="public/images/icons/logo-012.png" class="rounded-circle" id="output" style="width:250px;height:250px;object-fit:cover;" align="center" alt="">
                <input type="file" name="file" onchange="loadFile(event)" required align="center"><br>
                <script>
                    var loadFile = function(event) {
                        var image = document.getElementById('output');
                        image.src = URL.createObjectURL(event.target.files[0]);
                    };
                </script>
            </div>
            <span>Nama Lengkap</span><br>
            <input type="text" name="fullname" placeholder="Nama Lengkap" class="form-control" require><br>
            <span>Nomor KTP</span><br>
            <input type="number" name="noktp" placeholder="No. KTP" class="form-control" require><br>
            <span>Nomor HP</span><br>
            <input type="number" name="nohp" placeholder="No. HP" class="form-control" require><br>
            <span>Tanggal Lahir</span><br>
            <input type="date" name="tgllahir" placeholder="Tanggal Lahir" class="form-control" require><br>
            <span>Username</span><br>
            <input type="text" name="username" placeholder="Username" maxlength="25" class="form-control" require><br>
            <span>Kata Sandi</span><br>
            <input type="password" name="katasandi" placeholder="Kata Sandi" class="form-control" require><br>
            <span>Konfirmasi Kata Sandi</span><br>
            <input type="password" name="konfirmsandi" placeholder="Konfirmasi Kata Sandi" class="form-control" require><br>
            <span>Email</span><br>
            <input type="email" name="email" placeholder="Email" class="form-control" require><br>
            <span>Alamat</span><br>
            <textarea class="form-control" name="alamat" placeholder="Alamat" name="" id="" cols="30" rows="10" require></textarea><br>
            <input type="submit" name="submit" class="myButton centered" value="Simpan">
        </form>
    </div>
</section>