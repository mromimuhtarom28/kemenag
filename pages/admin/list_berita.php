<?php 
include('urlname.php');
if($filename === 'list_berita.php'): 
    include('block.php');
endif;
if($_SESSION['role_id'] == 2): 
    if(isset($_SESSION['id_login'])):
        $id_login = $_SESSION['id_login'];
        $condition = "AND berita.id_login = $id_login";
    endif;
elseif($_SESSION['role_id'] == 1):
    $condition = 'AND berita.status != 1';
endif;
if(isset($_POST['cari']))
{
    $usernamepandita = $_POST['username'];
    $mindate         = $_POST['mindate'];
    $maxdate         = $_POST['maxdate'];

    if($usernamepandita != NULL && $mindate != NULL && $maxdate != NULL): 
        if(is_numeric($usernamepandita)): 
            $queryselect = "SELECT 
                            pandita.id_pandita,
                            pandita.no_ktp,
                            login.username,
                            profil_login.nama_lengkap,
                            berita.judul,
                            berita.isi,
                            berita.tgl_terbit,
                            berita.status
                            FROM berita 
                            join login on login.id_login = berita.id_login
                            join pandita on pandita.id_profil = login.id_profil 
                            join profil_login on profil_login.id_profil = login.id_profil
                            where login.id_role = 2 
                            AND pandita.status = 2
                            AND no_ktp = $usernamepandita
                            AND berita.tgl_terbit >= $mindate
                            AND berita.tgl_terbit <= $maxdate
                            ".$condition;

            $hasil = $conn->query($queryselect);
        else: 
            $queryselect = "SELECT 
                            pandita.id_pandita,                          
                            pandita.no_ktp,
                            login.username,
                            profil_login.nama_lengkap,
                            berita.judul,
                            berita.isi,
                            berita.tgl_terbit,
                            berita.status
                            FROM berita 
                            join login on login.id_login = berita.id_login
                            join pandita on pandita.id_profil = login.id_profil 
                            join profil_login on profil_login.id_profil = login.id_profil 
                            where login.id_role = 2 
                            AND pandita.status = 2
                            AND username LIKE '%$usernamepandita%'
                            AND berita.tgl_terbit >= $mindate
                            AND berita.tgl_terbit <= $maxdate
                            ".$condition;

            $hasil = $conn->query($queryselect);
        endif;
    elseif($usernamepandita != NULL && $mindate != NULL):
        if(is_numeric($usernamepandita)): 
            $queryselect = "SELECT 
                            pandita.id_pandita,
                            pandita.no_ktp,
                            login.username,
                            profil_login.nama_lengkap,
                            berita.judul,
                            berita.isi,
                            berita.tgl_terbit,
                            berita.status
                            FROM berita  
                            join login on login.id_login = berita.id_login
                            join pandita on pandita.id_profil = login.id_profil 
                            join profil_login on profil_login.id_profil = login.id_profil
                            where login.id_role = 2 
                            AND pandita.status = 2
                            AND no_ktp = $usernamepandita
                            AND berita.tgl_terbit >= $mindate
                            ".$condition;

            $hasil = $conn->query($queryselect);
        else: 
            $queryselect = "SELECT 
                            pandita.id_pandita,
                            pandita.no_ktp,
                            login.username,
                            profil_login.nama_lengkap,
                            berita.judul,
                            berita.isi,
                            berita.tgl_terbit,
                            berita.status
                            FROM berita 
                            join login on login.id_login = berita.id_login
                            join pandita on pandita.id_profil = login.id_profil 
                            join profil_login on profil_login.id_profil = login.id_profil 
                            where login.id_role = 2 
                            AND pandita.status = 2
                            AND username LIKE '%$usernamepandita%'
                            AND berita.tgl_terbit >= $mindate]
                            ".$condition;

            $hasil = $conn->query($queryselect);
        endif;
    elseif($usernamepandita != NULL && $maxdate != NULL):
        if(is_numeric($usernamepandita)): 
            $queryselect = "SELECT
                            pandita.id_pandita, 
                            pandita.no_ktp,
                            login.username,
                            profil_login.nama_lengkap,
                            berita.judul,
                            berita.isi,
                            berita.tgl_terbit,
                            berita.status
                            FROM berita  
                            join login on login.id_login = berita.id_login
                            join pandita on pandita.id_profil = login.id_profil 
                            join profil_login on profil_login.id_profil = login.id_profil
                            where login.id_role = 2 
                            AND pandita.status = 2
                            AND no_ktp = $usernamepandita
                            AND berita.tgl_terbit <= $maxdate
                            ".$condition;

            $hasil = $conn->query($queryselect);
        else: 
            $queryselect = "SELECT
                            pandita.id_pandita, 
                            pandita.no_ktp,
                            login.username,
                            profil_login.nama_lengkap,
                            berita.judul,
                            berita.isi,
                            berita.tgl_terbit,
                            berita.status
                            FROM berita 
                            join login on login.id_login = berita.id_login
                            join pandita on pandita.id_profil = login.id_profil 
                            join profil_login on profil_login.id_profil = login.id_profil 
                            where login.id_role = 2 
                            AND pandita.status = 2
                            AND username LIKE '%$usernamepandita%'
                            AND berita.tgl_terbit <= $maxdate
                            ".$condition;

            $hasil = $conn->query($queryselect);
        endif;
    elseif($mindate != NULL && $maxdate != NULL):
            $queryselect = "SELECT
                            pandita.id_pandita, 
                            pandita.no_ktp,
                            login.username,
                            profil_login.nama_lengkap,
                            berita.judul,
                            berita.isi,
                            berita.tgl_terbit,
                            berita.status
                            FROM berita  
                            join login on login.id_login = berita.id_login
                            join pandita on pandita.id_profil = login.id_profil 
                            join profil_login on profil_login.id_profil = login.id_profil
                            where login.id_role = 2 
                            AND pandita.status = 2
                            AND berita.tgl_terbit >= $mindate
                            AND berita.tgl_terbit <= $maxdate
                            ".$condition;

            $hasil = $conn->query($queryselect);

    elseif($usernamepandita != NULL):
        if(is_numeric($usernamepandita)): 
            $queryselect = "SELECT
                            pandita.id_pandita,
                            pandita.no_ktp,
                            login.username,
                            profil_login.nama_lengkap,
                            berita.judul,
                            berita.isi,
                            berita.tgl_terbit,
                            berita.status
                            FROM berita 
                            join login on login.id_login = berita.id_login
                            join pandita on pandita.id_profil = login.id_profil 
                            join profil_login on profil_login.id_profil = login.id_profil
                            where login.id_role = 2 
                            AND pandita.status = 2
                            AND no_ktp = $usernamepandita
                            ".$condition;

            $hasil = $conn->query($queryselect);
        else: 
            $queryselect = "SELECT 
                            pandita.id_pandita,
                            pandita.no_ktp,
                            login.username,
                            profil_login.nama_lengkap,
                            berita.judul,
                            berita.isi,
                            berita.tgl_terbit,
                            berita.status
                            FROM berita
                            join login on login.id_login = berita.id_login
                            join pandita on pandita.id_profil = login.id_profil 
                            join profil_login on profil_login.id_profil = login.id_profil 
                            where login.id_role = 2 
                            AND pandita.status = 2
                            AND username LIKE '%$usernamepandita%'
                            ".$condition;

            $hasil = $conn->query($queryselect);
        endif;
    elseif($mindate != NULL):
        $queryselect = "SELECT 
                        pandita.id_pandita,
                        pandita.no_ktp,
                        login.username,
                        profil_login.nama_lengkap,
                        berita.judul,
                        berita.isi,
                        berita.tgl_terbit,
                        berita.status
                        FROM berita
                        join login on login.id_login = berita.id_login
                        join pandita on pandita.id_profil = login.id_profil 
                        join profil_login on profil_login.id_profil = login.id_profil
                        where login.id_role = 2 
                        AND pandita.status = 2
                        AND berita.tgl_terbit >= $mindate
                        ".$condition;

        $hasil = $conn->query($queryselect);
    elseif($maxdate != NULL):  
        $queryselect = "SELECT 
                        pandita.id_pandita,
                        pandita.no_ktp,
                        login.username,
                        profil_login.nama_lengkap,
                        berita.judul,
                        berita.isi,
                        berita.tgl_terbit,
                        berita.status
                        FROM berita
                        join pandita on pandita.id_profil = login.id_profil
                        join login on login.id_profil = profil_login.id_profil 
                        where login.id_role = 2 
                        AND pandita.status = 2
                        AND profil_login.ttl <= $maxdate
                        ".$condition;

        $hasil = $conn->query($queryselect);  
    else:  
        $queryselect = "SELECT 
                        pandita.id_pandita,
                        pandita.no_ktp,
                        login.username,
                        profil_login.nama_lengkap,
                        berita.judul,
                        berita.isi,
                        berita.tgl_terbit,
                        berita.status
                        FROM berita
                        join login on login.id_login = berita.id_login
                        join pandita on pandita.id_profil = login.id_profil  
                        join profil_login on profil_login.id_profil = login.id_profil
                        where login.id_role = 2 
                        AND pandita.status = 2
                        ".$condition;

        $hasil = $conn->query($queryselect);
    endif;     
} else {
    $queryselect = "SELECT 
                    pandita.id_pandita,
                    pandita.no_ktp,
                    login.username,
                    profil_login.nama_lengkap,
                    berita.judul,
                    berita.isi,
                    berita.tgl_terbit,
                    berita.status
                    FROM berita 
                    join login on login.id_login = berita.id_login
                    join pandita on pandita.id_profil = login.id_profil  
                    join profil_login on profil_login.id_profil = login.id_profil
                    where login.id_role = 2 
                    AND pandita.status = 2
                    ".$condition;

    $hasil = $conn->query($queryselect);
}

?>
<link rel="stylesheet" href="public/css/button.css">
<section class="bg0">
    <div class="container border" style="background-color:#DCDCDC;margin-bottom:20px;margin-top:20px;padding-top:50px;padding-bottom:50px;">
        <div align="center" style="margin-bottom:20px;">
            <h1><b>Laporan Acc Berita Pandita</b></h1>
        </div>
        <div>
        
        </div>

        <div class="border border-dark" align="center" style="margin-left:4px;margin-right:4px;padding:10px;">
            <form action="" method="post">
                <table border="0" width="100%">
                    <tr>
                        <td colspan="3" align="center">Pencarian Data</td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="3">No KTP / Username</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="text" name="username" class="form-control" placeholder="No KTP / Username" value="<?php if(isset($usernamepandita)): echo $usernamepandita; endif; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">Tanggal Pengajuan Berita</td>
                    </tr>
                    <tr>
                        <td>
                            <input type="date" name="mindate" class="form-control" value="<?php if(isset($mindate)): echo $mindate; endif; ?>">
                        </td>
                        <td>s/d</td>
                        <td>
                            <input type="date" name="maxdate" class="form-control" value="<?php if(isset($maxdate)): echo $maxdate; endif; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <button type="submit" class="btn btn-primary" name="cari">Cari</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div><br>
        

        <table width="100%" border="1">
            <tr>
                <td colspan="9" align="center">Table Tolak dan Terima Berita Pandita</td>
            </tr>
            <tr style="font-weight:bold">
                <td align="center">ID Pandita</td>
                <td align="center">No KTP</td>
                <td align="center">Username</td>
                <td align="center">Nama Lengkap</td>
                <td align="center">Judul</td>
                <td align="center">Isi</td>
                <td align="center">Tanggal Pengajuan Berita</td>
                <td align="center" colspan="2">status</td>
            </tr>
            <?php 
            if ($hasil->num_rows > 0):
                while ($pandita = $hasil->fetch_assoc()):
            ?>
            <tr>
                <td height="60px" align="center"><a href="index.php?page=detail_berita&id_berita=<?= $pandita['id_berita'] ?>"><?= $pandita['id_pandita'] ?></a></td>
                <td><?= $pandita['no_ktp'] ?></td>
                <td><?= $pandita['username'] ?></td>
                <td><?= $pandita['nama_lengkap'] ?></td>
                <td><?= $pandita['judul'] ?></td>
                <td>
                    <?php 
                        if($pandita['isi'] != NULL):
                    ?>
                        <?= cutText($pandita['isi'],70, 1); ?>. . . 
                    <?php
                        endif;
                    ?>
                </td>
                <td><?= $pandita['tgl_terbit'] ?></td>
                <td align="center" valign="middle">
                        <?php 
                            if($pandita['status'] == 0): 
                        ?>
                                <span style="color:red">Di Tolak</span>
                        <?php
                            elseif($pandita['status'] == 2): 
                        ?>
                                <span style="color:green">Di Terima</span> 
                        <?php
                            elseif($pandita['status'] == 1):
                        ?> 
                            <span style="color:blue">Menunggu Persetujuan</span>
                        <?php
                            endif;
                        ?>
                </td>
            </tr>
            <?php 
                endwhile;
            else: 
            ?>
            <tr>
                <td colspan="8" align="center">Data tidak ada</td>
            </tr>
            <?php
            endif;
            ?>
            <!-- <tr>
                <td>1</td>
                <td>217089766</td>
                <td>yolla</td>
                <td>Yolla Putri Ananda</td>
                <td>Tiban</td>
                <td>hhh</td>
                <td>20-11-1999</td>
                <td align="center">
                    <button type="button" class="btn btn-success">Terima</button>
                </td>
                <td align="center">
                    <button type="button" class="btn btn-danger">Tolak</button>
                </td>
            </tr> -->
        </table>
    </div>
</section>