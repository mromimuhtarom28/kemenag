<?php 
include('urlname.php');
if($filename === 'profile.php'): 
    include('block.php');
endif;
?>
<style>
    .img-circle {
        border-radius: 50%;
    }
</style>
<link rel="stylesheet" href="public/css/button.css">
<section class="bg0">
    <div class="container border" style="background-color:#DCDCDC;margin-bottom:20px;margin-top:20px;padding-top:50px;padding-bottom:50px;">
        <div align="center" style="margin-bottom:20px;">
            <h1><b>Profil Pribadi</b></h1>
        </div>
        <div>
        
        </div>

<?php 
    $userid = $_SESSION['userid'];
    if($_SESSION['role_id'] == 2):
        $queryselect = "SELECT * FROM login join profil_login on profil_login.id_profil = login.id_profil join pandita on pandita.id_profil = profil_login.id_profil where login.id_login = $userid";
        $hasil = $conn->query($queryselect);
        while ($data = $hasil->fetch_assoc())
        {
            // disertakan pula link untuk menghapus data berdasarkan ID nya
            $idprofil = $data['id_profil'];
            $username = $data['username'];
            $noktp    = $data['no_ktp'];
            $email    = $data['email'];
            $namalengkap = $data['nama_lengkap'];
            $alamat   = $data['alamat'];
            $ttl      = $data['ttl'];
            $telp     = $data['telp'];
            $idprofil = $data['id_profil'];
        }
    else:
        $queryselect = "SELECT * FROM login join profil_login on profil_login.id_profil = login.id_profil where login.id_login = $userid";
        $hasil = $conn->query($queryselect);
        while ($data = $hasil->fetch_assoc())
        {
            // disertakan pula link untuk menghapus data berdasarkan ID nya
            $idprofil = $data['id_profil'];
            $username = $data['username'];
            $namalengkap = $data['nama_lengkap'];
            $alamat   = $data['alamat'];
            $ttl      = $data['ttl'];
            $telp     = $data['telp'];
            $idprofil = $data['id_profil'];
        }
    endif;


?>

        <div align="center">
            <?php 
                $file = 'image/profil/'.$idprofil.'.png';
                if(file_exists($file)): 
                    $filename = $file;
                else: 
                    $filename = 'public/images/icons/logo-012.png';
                endif;
            ?>
            <img src="<?= $filename ?>" style="width:250px;height:250px;object-fit:cover;" align="center" class="img-circle" alt="">
        </div><br>

        <?php 
            if(isset($_SESSION["success"])):
        ?>
            <div class="alert alert-success">
                <?= $_SESSION["success"]; ?>
            </div>
            <?php unset($_SESSION['success']); ?>
        <?php 
            endif;

            if(isset($_SESSION['alert'])):
        ?>
            <div class="alert alert-danger">
                <?= $_SESSION["alert"]; ?>
            </div>
            <?php unset($_SESSION['alert']); ?> 
        <?php 
            endif;
        ?>
        <?php 
        if($_SESSION['role_id'] == 2):
        ?>
        <form action="process/editprofile.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="idprofil" value="<?= $idprofil ?>">
        <span>Nama Lengkap</span><br>
        <input type="text" name="fullname" placeholder="Nama Lengkap" class="form-control" value="<?= $namalengkap ?>" require><br>
        <span>Nomor KTP</span><br>
        <input type="number" name="noktp" placeholder="No. KTP" class="form-control" value="<?= $noktp ?>" require><br>
        <span>Nomor HP</span><br>
        <input type="number" name="nohp" placeholder="No. HP" class="form-control" value="<?= $telp ?>" require><br>
        <span>Tanggal Lahir</span><br>
        <input type="date" name="tgllahir" placeholder="Tanggal Lahir" class="form-control" value="<?= $ttl ?>" require><br>
        <span>Username</span><br>
        <input type="text" name="username" placeholder="Username" maxlength="25" class="form-control" value="<?= $username ?>" require><br>
        <span>Kata Sandi</span><br>
        <input type="password" name="katasandi" placeholder="Kata Sandi" class="form-control" require><br>
        <span>Konfirmasi Kata Sandi</span><br>
        <input type="password" name="konfirmsandi" placeholder="Konfirmasi Kata Sandi" class="form-control" require><br>
        <span>Email</span><br>
        <input type="email" name="email" placeholder="Email" class="form-control" value="<?= $email ?>" require><br>
        <span>Alamat</span><br>
        <textarea class="form-control" name="alamat" placeholder="Alamat" id="" cols="30" rows="10" require><?= $alamat ?></textarea><br>

        <button type="submit" name="submit" class="myButton centered">Edit</button>
        </form>
        <?php else: ?>
        <form action="process/editprofileadmin.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="idprofil" value="<?= $idprofil ?>">
        <span>Nama Lengkap</span><br>
        <input type="text" name="fullname" placeholder="Nama Lengkap" class="form-control" value="<?= $namalengkap ?>" require><br>
        <span>Nomor HP</span><br>
        <input type="number" name="nohp" placeholder="No. HP" class="form-control" value="<?= $telp ?>" require><br>
        <span>Tanggal Lahir</span><br>
        <input type="date" name="tgllahir" placeholder="Tanggal Lahir" class="form-control" value="<?= $ttl ?>" require><br>
        <span>Username</span><br>
        <input type="text" name="username" placeholder="Username" maxlength="25" class="form-control" value="<?= $username ?>" require><br>
        <span>Kata Sandi</span><br>
        <input type="password" name="katasandi" placeholder="Kata Sandi" class="form-control" require><br>
        <span>Konfirmasi Kata Sandi</span><br>
        <input type="password" name="konfirmsandi" placeholder="Konfirmasi Kata Sandi" class="form-control" require><br>
        <span>Alamat</span><br>
        <textarea class="form-control" name="alamat" placeholder="Alamat" id="" cols="30" rows="10" require><?= $alamat ?></textarea><br>

        <button type="submit" name="submit" class="myButton centered">Edit</button>
        </form>
        <?php endif; ?>
    </div>
</section>