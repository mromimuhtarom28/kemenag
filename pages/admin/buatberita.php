<?php 
    include('urlname.php');
    if($filename === 'buatberita.php'): 
        include('block.php');
    endif;
?>
<style>
.img-circle {
    border-radius: 50%;
}
</style>
<link rel="stylesheet" href="public/css/button.css">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<section class="bg0">
    <div class="container border" style="background-color:#DCDCDC;margin-bottom:20px;margin-top:20px;padding-top:50px;padding-bottom:50px;">
        <div align="center" style="margin-bottom:20px;">
            <h1><b>Buat Berita</b></h1>
        </div>
        <div>
        
        </div>
    <?php 
    if(isset($_SESSION['role_id'])):
        if($_SESSION['role_id'] == 1): 
            $url = 'process/insertberitaadmin.php';
        elseif($_SESSION['role_id'] == 2):
            $url = 'process/insertberitapandita.php';
        endif;
    endif;
    ?>
    <form action="<?= $url ?>" method="post" enctype="multipart/form-data">
        <div align="center">
            <img src="public/images/icons/logo-012.png" id="output" style="max-width:250px;maxheight:250px;" align="center" alt="">
            <input type="file" name="file" onchange="loadFile(event)">
            <script>
                var loadFile = function(event) {
                    var image = document.getElementById('output');
                    image.src = URL.createObjectURL(event.target.files[0]);
                };
            </script>
        </div><br>

        <?php 
            if(isset($_SESSION["success"])):
        ?>
            <div class="alert alert-success">
                <?= $_SESSION["success"]; ?>
            </div>
            <?php unset($_SESSION['success']); ?>
        <?php 
            endif;

            if(isset($_SESSION['alert'])):
        ?>
            <div class="alert alert-danger">
                <?= $_SESSION["alert"]; ?>
            </div>
            <?php unset($_SESSION['alert']); ?> 
        <?php 
            endif;
        ?>


        <span>Judul</span><br>
        <input type="text" name="judul" placeholder="Judul" class="form-control" require><br>
        <span>Headline</span><br>
        <input type="text" name="headline" maxlength="100" placeholder="Judul" class="form-control" require><br>
        <span>Kontent</span><br>
        <textarea class="form-control ckeditor" id="ckedtor" name="konten" placeholder="Konten" name="" cols="30" rows="10" require></textarea><br>
        <script>
            CKEDITOR.replace( 'konten', {
            removePlugins: 'bidi,dialogadvtab,div,filebrowser,flash,format,forms,horizontalrule,iframe,justify,liststyle,pagebreak,showborders,stylescombo,table,tabletools,templates',
            fontSize_sizes: "8/8px;9/9px;10/10px;11/11px;12/12px;14/14px;16/16px;18/18px;20/20px;22/22px;24/24px;26/26px;28/28px;36/36px;48/48px;72/72px",
            toolbar: [
                [ 'Undo', 'Redo' ],
                [ 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat' ],
                [ 'Link', 'Unlink', 'Smiley', 'SpecialChar', 'fontFamily', 'tables' ],
                '/',
                [ 'Bold', 'Italic', 'Paragraph' ],
                [ 'PasteFromWord', 'PasteText', 'PasteText', 'Cut', 'Strike', 'Table', 'BGColor' ],
                [ 'TextColor' ], 
                [ 'NumberedList', 'BulletedList', '-', 'Blockquote' ]
            ]
            });
            CKEDITOR.config.autoParagraph = false;
            CKEDITOR.config.coreStyles_bold = { element: 'b', overrides: 'strong' };
        </script>
        <button type="submit" name="submit" class="myButton centered">Tambah Artikel</button>
    </form>
    </div>
</section>