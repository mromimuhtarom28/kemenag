<?php 
    include('urlname.php');
    if($filename === 'admin.php'): 
        include('block.php');
    endif;

    if(isset($_POST['cari']))
    {
        $usernamepandita = $_POST['username'];
        $mindate         = $_POST['mindate'];
        $maxdate         = $_POST['maxdate'];
        
        if($usernamepandita != NULL && $mindate != NULL && $maxdate != NULL): 

                $queryselect = "SELECT * FROM profil_login  
                                join login on login.id_profil = profil_login.id_profil 
                                where login.id_role = 1
                                AND username LIKE '%$usernamepandita%'
                                AND profil_login.ttl >= $mindate
                                AND profil_login.ttl <= $maxdate
                                ";

                $hasil = $conn->query($queryselect);
        elseif($usernamepandita != NULL && $mindate != NULL):

                $queryselect = "SELECT * FROM profil_login 
                                join login on login.id_profil = profil_login.id_profil 
                                where login.id_role = 1 
                                AND username LIKE '%$usernamepandita%'
                                AND profil_login.ttl <= $maxdate
                                ";

                $hasil = $conn->query($queryselect);
        elseif($usernamepandita != NULL && $maxdate != NULL):

                $queryselect = "SELECT * FROM profil_login 
                                join login on login.id_profil = profil_login.id_profil 
                                where login.id_role = 1
                                AND username LIKE '%$usernamepandita%'
                                AND profil_login.ttl >= $mindate
                                ";

                $hasil = $conn->query($queryselect);
        elseif($mindate != NULL && $maxdate != NULL):
            $queryselect = "SELECT * FROM profil_login 
                            join login on login.id_profil = profil_login.id_profil 
                            where login.id_role = 1
                            AND profil_login.ttl >= $mindate
                            AND profil_login.ttl <= $maxdate
                            ";

            $hasil = $conn->query($queryselect);
  
        elseif($usernamepandita != NULL):


                $queryselect = "SELECT * FROM profil_login 
                                join login on login.id_profil = profil_login.id_profil 
                                where login.id_role = 1
                                AND username LIKE '%$usernamepandita%'
                                ";

                $hasil = $conn->query($queryselect);
        elseif($mindate != NULL):
            $queryselect = "SELECT * FROM profil_login 
                            join login on login.id_profil = profil_login.id_profil 
                            where login.id_role = 1
                            AND profil_login.ttl >= $mindate
                            ";

            $hasil = $conn->query($queryselect);
        elseif($maxdate != NULL):  
            $queryselect = "SELECT * FROM profil_login  
                            join login on login.id_profil = profil_login.id_profil 
                            where login.id_role = 1
                            AND profil_login.ttl <= $maxdate
                            ";

            $hasil = $conn->query($queryselect);  
        else:  
            $queryselect = "SELECT * FROM profil_login 
                        join login on login.id_profil = profil_login.id_profil 
                        where login.id_role = 1
                        ";

            $hasil = $conn->query($queryselect); 
        endif;     
    } else {
        $queryselect = "SELECT * FROM profil_login 
                        join login on login.id_profil = profil_login.id_profil 
                        where login.id_role = 1
                        ";

        $hasil = $conn->query($queryselect); 
    }
    
    
?>
<link rel="stylesheet" href="public/css/button.css">
<section class="bg0">
    <div class="container border" style="background-color:#DCDCDC;margin-bottom:20px;margin-top:20px;padding-top:50px;padding-bottom:50px;">
        <div align="center" style="margin-bottom:20px;">
            <h1><b>List Admin</b></h1>
        </div>
        <div>
        
        </div>

        <div class="border border-dark" align="center" style="margin-left:4px;margin-right:4px;padding:10px;">
            <form action="" method="post">
                <table border="0" width="100%">
                    <tr>
                        <td colspan="3" align="center">Pencarian Data</td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="3">Username</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="text" class="form-control" name="username" placeholder="No KTP / Username" value="<?php if(isset($usernamepandita)): echo $usernamepandita; endif; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">Tanggal Lahir</td>
                    </tr>
                    <tr>
                        <td>
                            <input type="date" class="form-control" name="mindate" value="<?php if(isset($mindate)): echo $mindate; endif; ?>">
                        </td>
                        <td>s/d</td>
                        <td>
                            <input type="date" class="form-control" name="maxdate" value="<?php if(isset($maxdate)): echo $maxdate; endif; ?>">
                        </td>
                        
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <button type="submit" class="btn btn-primary" name="cari">Cari</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div><br>
        <?php 
            if(isset($_SESSION["success"])):
        ?>
            <div class="alert alert-success">
                <?= $_SESSION["success"]; ?>
            </div>
            <?php unset($_SESSION['success']); ?>
        <?php 
            endif;

            if(isset($_SESSION['alert'])):
        ?>
            <div class="alert alert-danger">
                <?= $_SESSION["alert"]; ?>
            </div>
            <?php unset($_SESSION['alert']); ?> 
        <?php 
            endif;

        ?>

        <table width="100%" border="1">
            <tr>
                <td colspan="8" align="center">Table List Admin</td>
            </tr>
            <tr>
                <td colspan="8"><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#tambahAdmin">Tambah Admin</a></td>
            </tr>
            <tr style="font-weight:bold">
                <td align="center">ID Login</td>
                <td align="center">Username</td>
                <td align="center">Nama Lengkap</td>
                <td align="center">Alamat</td>
                <td align="center">Tanggal Lahir</td>
                <td align="center">Password</td>
                <td align="center">Aksi</td>
            </tr>
            <?php 
            if ($hasil->num_rows > 0):
                while ($pandita = $hasil->fetch_assoc()):

            ?>
            <tr>
                <td height="60px"><?= $pandita['id_login'] ?></td>
                <td><?= $pandita['username'] ?></td>
                <td><?= $pandita['nama_lengkap'] ?></td>
                <td><?= $pandita['alamat'] ?></td>
                <td><?= $pandita['ttl'] ?></td>
                <td><a href="#" class="btn btn-primary resetpwd" data-pk="<?= $pandita['id_profil'] ?>" data-toggle="modal" data-target="#resetPassword">Reset Password</a></td>
                <td align="center"><a href="#" data-pk="<?= $pandita['id_profil'] ?>" data-toggle="modal" data-target="#deleteAdmin" class="btn btn-danger delete-admin">Hapus</a></td>
            </tr>
            <?php 
                endwhile;
            else: 
            ?>
            <tr>
                <td colspan="7" align="center">Data tidak ada</td>
            </tr>
            <?php
            endif;
            ?>
        </table>
    </div>
</section>


<!-- Modal create -->
<div class="modal fade" id="tambahAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="process/admin/tambah.php" method="post">
      <div class="modal-body">
        <input type="text" name="username" class="form-control" placeholder="Username" required>
        <input type="password" name="passwordthisaccount" class="form-control" placeholder="Password yang sedang login" required>
        <input type="password" name="password" class="form-control" placeholder="Password untuk akun ini" required>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" name="tambah_admin" value="tambah">Tambah</button>
      </div>
      </form>
    </div>
  </div>
</div>


<!-- Modal Reset Password -->
<div class="modal fade" id="resetPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="process/admin/resetpwd.php" method="post">
        <div class="modal-body">
            <input type="hidden" name="pk" class="resetpwd-pk">
            <input type="password" name="passwordthisaccount" class="form-control" placeholder="Password yang sedang login" required>
            <input type="password" name="password" class="form-control" placeholder="Password untuk akun ini" required>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary" name="tambah_admin" value="tambah">Tambah</button>
        </div>
      </form>
    </div>
  </div>
</div>


<!-- Modal delete -->
<div class="modal fade" id="deleteAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="process/admin/delete.php" method="post">
      <div class="modal-body">
            <input type="hidden" class="delete-adm-pk" name="pk" >
            Apakah anda yakin ingin menghapus akun ini?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" name="delete_admin" value="delete">Hapus data</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script>
$('.delete-admin').on('click', function(){
    var pk = $(this).attr('data-pk');
    $('.delete-adm-pk').val(pk);
});

$('.resetpwd').on('click', function(){
    var pk = $(this).attr('data-pk');
    $('.resetpwd-pk').val(pk);
});
</script>