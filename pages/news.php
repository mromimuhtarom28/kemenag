<?php 
	$halaman = 10;                                                //batasan halaman
	$page    = isset($_GET['halaman'])? (int)$_GET["halaman"]:1;
	$mulai   = ($page>1) ? ($page * $halaman) - $halaman : 0;
    $queryallnews = "SELECT * FROM berita 
									join login on login.id_login = berita.id_login
									left join pandita on pandita.id_profil = login.id_profil  
									join profil_login on profil_login.id_profil = login.id_profil
									AND berita.status = 2
									ORDER BY id_berita DESC LIMIT ".$mulai.", ".$halaman;

	$allberita     		    = $conn->query($queryallnews); 
	$allberitanopag         = "SELECT * FROM berita 
								join login on login.id_login = berita.id_login
								left join pandita on pandita.id_profil = login.id_profil  
								join profil_login on profil_login.id_profil = login.id_profil
								AND berita.status = 2
								ORDER BY id_berita DESC";
	$sql                    = $conn->query($allberitanopag);
	$total = mysqli_num_rows($sql);
	$pages = ceil($total/$halaman); 


	$queryallnew = "SELECT * FROM berita 
					join login on login.id_login = berita.id_login
					left join pandita on pandita.id_profil = login.id_profil  
					join profil_login on profil_login.id_profil = login.id_profil
					AND berita.status = 2
					ORDER BY id_berita DESC
					LIMIT 5";

	$allnew    		    = $conn->query($queryallnew); 
?>

	<section class="bg0 p-t-70 p-b-55">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10 col-lg-8 p-b-80">
					<div class="row">
						<?php 
							while ($beritaall = $allberita->fetch_assoc()):
						?>
							<div class="col-sm-6 p-r-25 p-r-15-sr991">
								<!-- Item latest -->	
								<div class="m-b-45">
									<a href="index.php?page=detail_berita&id_berita=<?= $beritaall['id_berita'] ?>" align="center" style="background-color:#a4a4a4;height: 200px;border-radius:5px" class="wrap-pic-w hov1 trans-03">
										<img src="image/berita_pandita/<?= $beritaall['gambar']; ?>" style="border-radius:5px" alt="IMG">
									</a>

									<div class="p-t-16">
										<h5 class="p-b-5">
											<a href="index.php?page=detail_berita&id_berita=<?= $beritaall['id_berita'] ?>" class="f1-m-3 cl2 hov-cl10 trans-03">
												<?= $beritaall['judul'] ?>
											</a>
										</h5>

										<span class="cl8">
											<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
											<?= $beritaall['nama_lengkap'] ?>
											</a>

											<span class="f1-s-3 m-rl-3">
												-
											</span>

											<span class="f1-s-3">
												<?= date_format(date_create($beritaall['tgl_terbit']),"d M Y"); ?>
											</span>
										</span>
									</div>
								</div>
							</div>
							<?php endwhile; ?>

					</div>

					<!-- Pagination -->
					
					<div class="flex-wr-s-c m-rl--7 p-t-15">
					<?php 
						if(isset($_GET['halaman'])):
						$getpage = $_GET['halaman'];
						endif;
					?>
					<?php for ($i=1; $i<=$pages ; $i++): ?>
					<a href="?page=news&halaman=<?php echo $i; ?>" class="flex-c-c pagi-item hov-btn1 trans-03 m-all-7 <?php if($getpage == $i): ?>pagi-active<?php endif; ?>"><?php echo $i; ?></a>
					<?php endfor; ?>
					</div>
				</div>

				<div class="col-md-10 col-lg-4 p-b-80">
					<div class="p-l-10 p-rl-0-sr991">							


						<!-- Most Popular -->
						<div class="p-b-23">
							<div class="how2 how2-cl4 flex-s-c">
								<h3 class="f1-m-2 cl3 tab01-title">
									Terbaru
								</h3>
							</div>

							<ul class="p-t-35">
								<?php 
									$a = 1;
									while ($beritaall = $allnew->fetch_assoc()):
								?>
								<li class="flex-wr-sb-s p-b-22">
									<div class="size-a-8 flex-c-c borad-3 size-a-8 bg9 f1-m-4 cl0 m-b-6">
										<?= $a; ?>
									</div>

									<a href="#" class="size-w-3 f1-s-7 cl3 hov-cl10 trans-03">
										<?= $beritaall['judul'] ?>
									</a>
								</li>
								<?php 
									$a++;
									endwhile; 
								?>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>