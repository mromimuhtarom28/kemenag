<?php 
$id = $_GET['id_berita'];
if(isset($_GET['back'])):
$queryselectberitalimitone = "SELECT * FROM berita 
							join login on login.id_login = berita.id_login
							left join pandita on pandita.id_profil = login.id_profil  
							join profil_login on profil_login.id_profil = login.id_profil
							AND berita.status = 1
							AND id_berita = $id
							ORDER BY id_berita DESC 
							limit 1";
else: 
	$queryselectberitalimitone = "SELECT * FROM berita 
							join login on login.id_login = berita.id_login
							left join pandita on pandita.id_profil = login.id_profil  
							join profil_login on profil_login.id_profil = login.id_profil
							AND berita.status = 2
							AND id_berita = $id
							ORDER BY id_berita DESC 
							limit 1";
endif;

$hasilone      		    = $conn->query($queryselectberitalimitone); 

while ($beritaone = $hasilone->fetch_assoc()):
	$nama_pembuat = $beritaone['nama_lengkap'];
	$judul        = $beritaone['judul'];
	$isi          = $beritaone['isi'];
	$tgl_terbit   = $beritaone['tgl_terbit'];
	$gambar       = $beritaone['gambar'];
endwhile;
?>
<section class="bg0 p-b-70 p-t-5">
		<!-- Title -->
		<div class="bg-img1 size-a-18 how-overlay1" style="background-image: url(image/berita_pandita/<?= $gambar; ?>);">
			<div class="container h-full flex-col-e-c p-b-58">
				<h3 class="f1-l-5 cl0 p-b-16 txt-center respon2">
                    <?= $judul ?>
				</h3>

				<div class="flex-wr-c-s">
					<span class="f1-s-3 cl8 m-rl-7 txt-center">
						<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
							<?= $nama_pembuat ?>
						</a>

						<span class="m-rl-3">-</span>

						<span>
							<?= date_format(date_create($tgl_terbit),"Y-M-d H:i:s"); ?>
						</span>
					</span>
				</div>
			</div>
		</div>

		<!-- Detail -->
		<div class="container p-t-82">
			<div class="row justify-content-center">
				<div class="col-md-10 col-lg-8 p-b-100">
					<div class="p-r-10 p-r-0-sr991">
						<!-- Blog Detail -->
						<div class="p-b-70">
							<?= $isi ?>
						</div>

					</div>
				</div>
				
			</div>
		</div>
	</section>