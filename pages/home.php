<?php
    $queryselectberitalimitone = "SELECT * FROM berita 
								join login on login.id_login = berita.id_login
								left join pandita on pandita.id_profil = login.id_profil  
								join profil_login on profil_login.id_profil = login.id_profil
								AND berita.status = 2
								ORDER BY id_berita DESC 
								limit 1";

	$hasilone      		    = $conn->query($queryselectberitalimitone); 

	$queryselectlimitsecond = "SELECT * FROM berita 
							  join login on login.id_login = berita.id_login
							  left join pandita on pandita.id_profil = login.id_profil  
							  join profil_login on profil_login.id_profil = login.id_profil
							  AND berita.status = 2
							  ORDER BY id_berita DESC 
							  LIMIT 1 
							  OFFSET 1";
	$hasilsecond	        = $conn->query($queryselectlimitsecond);

	$queryselectlimitthird = "SELECT * FROM berita 
								join login on login.id_login = berita.id_login
								left join pandita on pandita.id_profil = login.id_profil  
								join profil_login on profil_login.id_profil = login.id_profil
								AND berita.status = 2
								ORDER BY id_berita DESC 
								LIMIT 1 
								OFFSET 2";
	$hasilthird	        	= $conn->query($queryselectlimitthird);

	$queryselectlimitfourth = "SELECT * FROM berita 
								join login on login.id_login = berita.id_login
								left join pandita on pandita.id_profil = login.id_profil  
								join profil_login on profil_login.id_profil = login.id_profil
								AND berita.status = 2
								ORDER BY id_berita DESC 
								LIMIT 1 
								OFFSET 3";
	$hasilfourth	        	= $conn->query($queryselectlimitfourth);


	$queryallnew = "SELECT * FROM berita 
					join login on login.id_login = berita.id_login
					left join pandita on pandita.id_profil = login.id_profil  
					join profil_login on profil_login.id_profil = login.id_profil
					AND berita.status = 2
					ORDER BY id_berita DESC
					LIMIT 5";

	$allnew    		    = $conn->query($queryallnew); 
?>
	<div class="container">
		<div class="bg0 flex-wr-sb-c p-rl-20 p-tb-8">
			<div class="f2-s-1 p-r-30 size-w-0 m-tb-6 flex-wr-s-c">
				<span class="text-uppercase cl2 p-r-8">
					Berita Terbaru:
				</span>

				<span class="dis-inline-block cl6 slide100-txt pos-relative size-w-0" data-in="fadeInDown"
					data-out="fadeOutDown">
					<?php 
					while ($judulberita = $allnew->fetch_assoc()):
					?>
					<span class="dis-inline-block slide100-txt-item animated visible-false">
						<?= $judulberita['judul']; ?>
					</span>
					<?php endwhile; ?>
					
				</span>
			</div>
		</div>
	</div>

		<section class="bg0">
		<div class="container">
			<div class="row m-rl--1">
				<?php 
					while ($beritaone = $hasilone->fetch_assoc()):
				?>
				<div class="col-md-6 p-rl-1 p-b-2">
					<div class="bg-img1 size-a-3 how1 pos-relative" style="background-image: url(image/berita_pandita/<?= $beritaone['gambar']; ?>);">
						<a href="index.php?page=detail_berita&id_berita=<?= $beritaone['id_berita'] ?>" class="dis-block how1-child1 trans-03"></a>

						<div class="flex-col-e-s s-full p-rl-25 p-tb-20">
							<h3 class="how1-child2 m-t-14 m-b-10">
								<a href="index.php?page=detail_berita&id_berita=<?= $beritaone['id_berita'] ?>" class="how-txt1 size-a-6 f1-l-1 cl0 hov-cl10 trans-03">
								<?= $beritaone['judul']?>
								</a>
							</h3>

							<span class="how1-child2">
								<span class="f1-s-4 cl11">
									<?= $beritaone['nama_lengkap'] ?>
								</span>

								<span class="f1-s-3 cl11 m-rl-3">
									-
								</span>

								<span class="f1-s-3 cl11">
									<?= date_format(date_create($beritaone['tgl_terbit']),"Y-M-d H:i:s"); ?>
								</span>
							</span>
						</div>
					</div>
				</div>
				<?php endwhile; ?>

				<div class="col-md-6 p-rl-1">
					<div class="row m-rl--1">
					<?php 
						while ($beritasecond = $hasilsecond->fetch_assoc()):
					?>
						<div class="col-12 p-rl-1 p-b-2">
							<div class="bg-img1 size-a-4 how1 pos-relative"
								style="background-image: url(image/berita_pandita/<?= $beritasecond['gambar']; ?>);">
								<a href="index.php?page=detail_berita&id_berita=<?= $beritasecond['id_berita'] ?>" class="dis-block how1-child1 trans-03"></a>

								<div class="flex-col-e-s s-full p-rl-25 p-tb-24">
									<h3 class="how1-child2 m-t-14">
										<a href="index.php?page=detail_berita&id_berita=<?= $beritasecond['id_berita'] ?>"
											class="how-txt1 size-a-7 f1-l-2 cl0 hov-cl10 trans-03">
											<?= $beritasecond['judul']; ?>
										</a>
									</h3>

									<span class="how1-child2">
										<span class="f1-s-4 cl11">
										<?= $beritasecond['nama_lengkap'] ?>
										</span>

										<span class="f1-s-3 cl11 m-rl-3">
											-
										</span>

										<span class="f1-s-3 cl11">
											<?= date_format(date_create($beritasecond['tgl_terbit']),"Y-M-d H:i:s"); ?>
										</span>
									</span>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
					<?php 
						while ($beritathird = $hasilthird->fetch_assoc()):
					?>
						<div class="col-sm-6 p-rl-1 p-b-2">
							<div class="bg-img1 size-a-5 how1 pos-relative"
								style="background-image: url(image/berita_pandita/<?= $beritathird['gambar']; ?>);">
								<a href="index.php?page=detail_berita&id_berita=<?= $beritathird['id_berita'] ?>" class="dis-block how1-child1 trans-03"></a>

								<div class="flex-col-e-s s-full p-rl-25 p-tb-20">
									<h3 class="how1-child2 m-t-14">
										<a href="index.php?page=detail_berita&id_berita=<?= $beritathird['id_berita'] ?>"
											class="how-txt1 size-h-1 f1-m-1 cl0 hov-cl10 trans-03">
											<?= $beritathird['judul']; ?>
										</a>
									</h3>
									<span class="how1-child2">
										<span class="f1-s-4 cl11">
											<?= $beritathird['nama_lengkap'] ?>
										</span>

										<span class="f1-s-3 cl11 m-rl-3">
											-
										</span>

										<span class="f1-s-3 cl11">
											<?= date_format(date_create($beritathird['tgl_terbit']),"Y-M-d H:i:s"); ?>
										</span>
									</span>
								</div>
							</div>
						</div>
					<?php endwhile; 
						while ($beritafourth = $hasilfourth->fetch_assoc()):
					?>
						<div class="col-sm-6 p-rl-1 p-b-2">
							<div class="bg-img1 size-a-5 how1 pos-relative"
								style="background-image: url(image/berita_pandita/<?= $beritafourth['gambar']; ?>);">
								<a href="index.php?page=detail_berita&id_berita=<?= $beritafourth['id_berita'] ?>" class="dis-block how1-child1 trans-03"></a>

								<div class="flex-col-e-s s-full p-rl-25 p-tb-20">
									<h3 class="how1-child2 m-t-14">
										<a href="index.php?page=detail_berita&id_berita=<?= $beritafourth['id_berita'] ?>"
											class="how-txt1 size-h-1 f1-m-1 cl0 hov-cl10 trans-03">
											<?= $beritafourth['judul']; ?>
										</a>
									</h3>
									<span class="how1-child2">
										<span class="f1-s-4 cl11">
											<?= $beritafourth['nama_lengkap'] ?>
										</span>

										<span class="f1-s-3 cl11 m-rl-3">
											-
										</span>

										<span class="f1-s-3 cl11">
											<?= date_format(date_create($beritafourth['tgl_terbit']),"Y-M-d H:i:s"); ?>
										</span>
									</span>
								</div>
							</div>
						</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</section>